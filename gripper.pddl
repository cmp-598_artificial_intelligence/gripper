﻿(define (problem gripper-four)
    (:domain gripper)

;; Griṕper task: Objects
;; OBJECTS:
;; Rooms: room_a, room_b
;; Balls: ball1, ball2, ball3, ball4
;; Robot arms: left, right
    (:objects room_a room_b
        ball1 ball2 ball3 ball4
        left right)

;; Griṕper task: Initial state
;; Initial state:
;; ROOM(room_a) and ROOM(room_b) are true.
;; BALL(ball1),..., BALL(ball4) are true.
;; GRIPPER(left), GRIPPER(right), free(left) and free(right) are true.
;; at-robby(room_a), at-ball(ball1, room_a), ..., at-ball(ball4, room_a) are true.
;; Everything else is false
    (:init 
        (ROOM room_a) (ROOM room_b)
        (BALL ball1) (BALL ball2) (BALL ball3) (BALL ball4)
        (GRIPPER left) (GRIPPER right) (free left) (free right)
        (at-robby room_a)
        (at-ball ball1 room_a) (at-ball ball2 room_a)
        (at-ball ball3 room_a) (at-ball ball4 room_a)
    )

;; Griṕper task: Goal especification
;; Goal especification:
;; at-ball(ball1, room_b), ..., at-ball(ball4, room_b) must be true.
;; Everything else we don't care about.
    (:goal
        (and
            (at-ball ball1 room_b)
            (at-ball ball2 room_b)
            (at-ball ball3 room_b)
            (at-ball ball4 room_b)
        )
    )

)
