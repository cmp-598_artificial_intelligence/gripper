﻿;; Gripper task with four balls:
;;
;; There is a robot that can move between two rooms and pick up or drop balls
;; with either of his two arms. Initially, all balls and the robot are in the first room.
;; We want the balls to be in the second room.
;;
;; Objects: The two rooms, four balls and two robot arms.
;; Predicates: Is x a room? Is x a ball? Is ball x inside room y? Is robot arm x empty?
;; Initial state: All balls and the robot are in the first room. All robot arms are empty.
;; Goal specification: All balls and must be in the second room.
;; Actions/Operators: The robot can move between rooms, pick up a ball or drop a ball.

(define (domain gripper)
    (:requirements :strips)


;; Gripper task: Predicates
;; Predicates:
;; ROOM(x)       -true iff x is a room
;; BALL(x)       -true iff x is a ball
;; GRIPPER(x)    -true iff x is a gripper (robot arm)
;; at-robby(x)   -true iff x is a room and the robot is in x
;; at-ball(x, y) -true iff x is a ball, y is a room, and x is in y
;; free(x)       -true iff x is a gripper and x does not hold a ball
;; carry(x, y)   -true iff is a gripper, y is a ball, and x holds y

    (:predicates
        (ROOM ?r)
        (BALL ?b)
        (GRIPPER ?g)
        (at-robby ?r)
        (at-ball ?b ?r)
        (free ?g)
        (carry ?o ?g)
    )

;; Gripper task: Movement operator
;; Action/Operator
;; Description: The robot can move from x to y.
;; Precondition: ROOM(x), ROOM(y) and at-robby(x) are true
;; Effect: at-robby(y) becomes true. at-robby(x) becomes false.
;; Everything else doesn't change.

    (:action move 
        :parameters (?from ?to)
        :precondition (and (ROOM ?from) (ROOM ?to) (at-robby ?from))
        :effect (and (at-robby ?to) (not (at-robby ?from)))
    )

;; Gripper task: Pick-up operator
;; Action/Operator
;; Description: The robot can pick up x in y with z.
;; Precondition: BALL(X), ROOM(y), GRIPPER(z), at-ball(x, y), 
;;               at-robby(x) and free(z) are true.
;; Effect: carry(z, x) becomes true. at-ball(x, y) and free(z) become false.
;; Everything else doesn't change.

    (:action pick-up 
        :parameters (?obj ?room ?gripper)
        :precondition (and (BALL ?obj) (ROOM ?room) (GRIPPER ?gripper) (at-ball ?obj ?room) (at-robby ?room) (free ?gripper))
        :effect (and (carry ?obj ?gripper) (not (at-ball ?obj ?room)) (not (free ?gripper)))
    )

;; Gripper task: Drop operator
;; Action/Operator
;; Description: The robot can drop x in y from z.
;; ( Precondition and effects similar to the pick-up operator.)

    (:action drop 
        :parameters (?obj ?room ?gripper)
        :precondition (and (BALL ?obj) (ROOM ?room) (GRIPPER ?gripper) (carry ?obj ?gripper) (at-robby ?room))
        :effect (and (at-ball ?obj ?room) (free ?gripper) (not (carry ?obj ?gripper)))
    )

)
